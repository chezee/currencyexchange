//
//  CalculatorViewController.swift
//  CurrencyExchangeSwift
//
//  Created by Ilya on 13/3/17.
//  Copyright © 2017 Ilya. All rights reserved.
//
import UIKit


class CalculatorViewController: UIViewController {

    var model: FetchModel = FetchModel.shared()
    
    var uahLabel: UILabel = {
        let label = UILabel.init(frame: CGRect(x: 40, y: 50, width: 260, height: 50))
        label.text = "0"
        label.textColor = UIColor.CurrencyColor.fontBlueColor
        label.font = UILabel().font.withSize(30.0)
        label.textAlignment = .center
        return label
    }()
    
    var uah: UILabel = {
        let label = UILabel.init(frame: CGRect(x: 15, y: 50, width: 25, height: 50))
        label.text = "₴"
        label.textColor = UIColor.CurrencyColor.currencySign
        label.font = UILabel().font.withSize(30.0)
        label.textAlignment = .center
        return label
    }()
    
    var usd: UILabel = {
        let label = UILabel.init(frame: CGRect(x: 15, y: 100, width: 25, height: 50))
        label.text = "$"
        label.textColor = UIColor.CurrencyColor.currencySign
        label.font = UILabel().font.withSize(30.0)
        label.textAlignment = .center
        return label
    }()
    
    var eur: UILabel = {
        let label = UILabel.init(frame: CGRect(x: 15, y: 150, width: 25, height: 50))
        label.text = "€"
        label.textColor = UIColor.CurrencyColor.currencySign
        label.font = UILabel().font.withSize(30.0)
        label.textAlignment = .center
        return label
    }()
    
    var cny: UILabel = {
        let label = UILabel.init(frame: CGRect(x: 15, y: 200, width: 25, height: 50))
        label.text = "¥"
        label.textColor = UIColor.CurrencyColor.currencySign
        label.font = UILabel().font.withSize(30.0)
        label.textAlignment = .center
        return label
    }()
    var usdLabel: UILabel = UILabel()
    var eurLabel: UILabel = UILabel()
    var cnyLabel: UILabel = UILabel()

    var labelArray: Array<UILabel> = Array()
    var arrayWithObjects: [Double] = []
    
    
    @IBOutlet weak var acButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        model = FetchModel.shared()
        arrayWithObjects = [0.0, model.rates["USD"]![0], model.rates["EUR"]![0], model.rates["CNY"]![0]]
        
        print(arrayWithObjects)
        usdLabel = UILabel.init(frame: CGRect(x: 40, y: 100, width: 260, height: 50))
        usdLabel.text = String(model.rates["USD"]![0])
        usdLabel.textColor = UIColor.CurrencyColor.fontBlueColor
        usdLabel.font = UILabel().font.withSize(30.0)
        usdLabel.textAlignment = .center

        eurLabel = UILabel.init(frame: CGRect(x: 40, y: 150, width: 260, height: 50))
        eurLabel.text = String(model.rates["EUR"]![0])
        eurLabel.textColor = UIColor.CurrencyColor.fontBlueColor
        eurLabel.font = UILabel().font.withSize(30.0)
        eurLabel.textAlignment = .center
        
        cnyLabel = UILabel.init(frame: CGRect(x: 40, y: 200, width: 260, height: 50))
        cnyLabel.text = String(model.rates["CNY"]![0])
        cnyLabel.textColor = UIColor.CurrencyColor.fontBlueColor
        cnyLabel.font = UILabel().font.withSize(30.0)
        cnyLabel.textAlignment = .center
        
        view.backgroundColor = UIColor.CurrencyColor.backgroundDarkColor
        labelArray = [uahLabel, usdLabel, eurLabel, cnyLabel, uah, usd, eur, cny]
        for item in labelArray {
            self.view.addSubview(item)
        }
        acButton.addTarget(self, action: #selector(acButtonPressed), for: UIControlEvents.touchUpInside)        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print(arrayWithObjects)
        arrayWithObjects = [0.0, model.rates["USD"]![0], model.rates["EUR"]![0], model.rates["CNY"]![0]]
    }
    
    @IBAction func clickDigit(sender: UIButton) {
        let digit: UInt64 = UInt64(sender.tag)
        self.processDigit(digit: digit)
    }
    
    func processDigit(digit: UInt64) {
        let cacheText = labelArray[0].text
        var value: UInt64 = UInt64(cacheText!)!
        if value < 99999999999 {
            value = (value * 10 + digit)
            labelArray[0].text = String(value)
        
            for index in 1...3 {
                let cacheText = labelArray[0].text
                var value = Double(cacheText!)!
                value = value / arrayWithObjects[index]
                value = value.roundTo(places: 2)
                labelArray[index].text = String(value)
            }
        } else {
        }
    }
    
    func acButtonPressed() {
        uahLabel.text = "0"
        usdLabel.text = String(arrayWithObjects[1])
        eurLabel.text = String(arrayWithObjects[2])
        cnyLabel.text = String(arrayWithObjects[3])
    }
}
