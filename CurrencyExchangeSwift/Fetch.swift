import UIKit
import Alamofire

class FetchModel: NSObject {
    enum FetchSource: Int {
        case nationalBank = 0
        case blackMarket = 1
        case bank = 2
        case interBank = 3
    }
    
    private override init() {
        super.init(source = .nationalBank)
        self.fetchData(currency: ["USD", "EUR", "CNY"])
    }
    
    private static var uniqueInstance: FetchModel?
    
    static func shared() -> FetchModel {
        if uniqueInstance == nil {
            uniqueInstance = FetchModel()
        }
        return uniqueInstance!
    }
    
    var source: FetchSource
    var usdArray: Array<Double> = []
    var eurArray: Array<Double> = []
    var cnyArray: Array<Double> = []
    var currency: String = ""
    var rates = [String: [Double]]()
    var buy: Double = 0.0
    var sell: Double = 0.0
    var dateOfFetch: String = ""
    
    func fetchGraphData(currency: String) -> Array<Double> {
        var fetchedArray: Array<Double> = Array()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd,hhmm"
        let date = formatter.string(from: Date())
        var dateArray = date.components(separatedBy: ",")
        var url: URL
        for item in 0...6 {
            if Int(dateArray[1])! > 900 {
                url = URL(string: String(format: "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?&valcode=%@&date=%i&json", currency, Int(dateArray[0])! - item))!
            } else {
                url = URL(string: String(format: "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?&valcode=%@&date=%i&json", currency, Int(dateArray[0])! - 1 - item))!
            }
            let json = try! String(contentsOf: url, encoding: String.Encoding.utf8)
            let jsonData: Data = json.data(using: String.Encoding.utf8)!
            let array = try! JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
            var rate: Double = (array[0] as AnyObject).value(forKey: "rate") as! Double
            rate = rate.roundTo(places: 2)
            fetchedArray.append(rate)
        }
        return fetchedArray
    }
    
    func fetchData(currency: [String]){
        if source.rawValue == 0 {
            fetchFromNationalBank(currency: currency)
        }
        else if source.rawValue == 1 {
                
        }
        else if source.rawValue == 2 {
            fetchFromPrivat(currency: currency)
        }
        else {
            fetchForInterbank(currency: currency)
        }
    }
    
    func fetchFromNationalBank(currency: [String]) {
        var url: URL
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd,hhmm"
        dateOfFetch = formatter.string(from: Date())
        var dateArray = dateOfFetch.components(separatedBy: ",")
        for item in currency {
        if Int(dateArray[1])! > 900 {
            url = URL(string: String(format: "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?&valcode=%@&date=%@&json", item, dateArray[0]))!
        } else {
            url = URL(string: String(format: "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?&valcode=%@&date=%i&json", item, Int(dateArray[0])! - 1))!
        }
        let json = try! String(contentsOf: url, encoding: String.Encoding.utf8)
        let jsonData: Data = json.data(using: String.Encoding.utf8)!
        let array = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! NSArray
        buy = (array[0] as AnyObject).value(forKey: "rate") as! Double
        self.rates[item] = [buy.roundTo(places: 2), buy.roundTo(places: 2)]
        }
        formatter.dateFormat = "yyyy/MM/dd hh:mm"
        dateOfFetch = formatter.string(from: Date())
    }
    
    func fetchFromPrivat(currency: [String]) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd hh:mm"
        dateOfFetch = formatter.string(from: Date())
        var buy: Double = 0.0
        var sell: Double = 0.0
        let url: URL = URL(string: String(format: "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5"))!
        let json = try! String(contentsOf: url)
        let jsonData: Data  = json.data(using: String.Encoding.utf8)!
        let array = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! Array<[String:Any]>
        for item in currency {
            if item == "EUR" {
                let buyS: String = array[0]["buy"] as! String
                let sellS: String = array[0]["sale"] as! String
                buy =  Double(buyS)!.roundTo(places: 2)
                sell = Double(sellS)!.roundTo(places: 2)
                self.rates[item] = [buy, sell]
            } else if item == "USD" {
                let buyS: String = array[2]["buy"] as! String
                let sellS: String = array[2]["sale"] as! String
                buy =  Double(buyS)!.roundTo(places: 2)
                sell = Double(sellS)!.roundTo(places: 2)
                self.rates[item] = [buy,sell]
            } else {
                let buyS: String = array[1]["buy"] as! String
                let sellS: String = array[1]["sale"] as! String
                buy =  Double(buyS)!.roundTo(places: 2)
                sell = Double(sellS)!.roundTo(places: 2)
                self.rates[item] = [buy, sell]
            }
        }
    }
    
    func fetchForInterbank(currency: [String]) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd hh:mm"
        dateOfFetch = formatter.string(from: Date())
        let url: URL = URL(string: "http://openrates.in.ua/rates")!
        let json = try! String(contentsOf: url)
        let data: Data = json.data(using: String.Encoding.utf8)!
        let array = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:[String:[String:Double]]]
        for item in currency {
        buy = array[item]!["interbank"]!["buy"]!
        sell = array[item]!["interbank"]!["sell"]!
        self.rates[item] = [buy, sell]
        }
    }
    
    func fetchFromNBU(currency: String) {
        Alamofire.request("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?&valcode=USD&json").responseJSON { response in
            print("Request: \(response.request)")
            print("Response: \(response.response)")
            print("Error: \(response.error)")
            print("Data: \(response.data)")     // server data
            print("Result: \(response.result)")   // result of response serialization
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
            }
        }
    }
    
}
