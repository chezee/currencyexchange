//
//  ViewController.swift
//  CurrencyExchangeSwift
//
//  Created by Ilya on 13/3/17.
//  Copyright © 2017 Ilya. All rights reserved.
//

import UIKit

extension PageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if viewController.isKind(of: GraphViewController.self) {
            return nil
        } else if viewController.isKind(of: CurrencyViewController.self) {
            return getGraphView()
        } else if viewController.isKind(of: CalculatorViewController.self) {
            return getCurrencyView()
        } else {
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController.isKind(of: CalculatorViewController.self) {
            return nil
        } else if viewController.isKind(of: CurrencyViewController.self) {
            return getCalcView()
        } else if viewController.isKind(of: GraphViewController.self) {
            return getCurrencyView()
        } else {
            return nil
        }
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 3
    }
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 1
    }
    
}

class PageViewController: UIPageViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.sizeThatFits(CGSize(width: 60, height: 60))
        dataSource = self
        view.backgroundColor = UIColor(ciColor: CIColor(red: 0.070588, green: 0.070588, blue: 0.1098))
        
        setViewControllers([getCurrencyView()], direction: .forward, animated: false, completion: nil)
        
    }
    
    func getCurrencyView() -> CurrencyViewController {
        return self.storyboard!.instantiateViewController(withIdentifier: "CurrencyView") as! CurrencyViewController
    }
    func getGraphView() -> GraphViewController {
        return storyboard!.instantiateViewController(withIdentifier: "GraphView") as! GraphViewController
    }
    func getCalcView() -> CalculatorViewController {
        return storyboard!.instantiateViewController(withIdentifier: "CalcView") as! CalculatorViewController
    }
}

