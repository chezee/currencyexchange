import UIKit
import CFAlertViewController

class CurrencyViewController: UIViewController {
    
    var fetch: FetchModel = FetchModel.shared()
    var firstCurrency: String = "USD"
    var secondCurrency: String = "EUR"
    var thirdCurrency: String = "CNY"
    var USDlabel = UILabel()
    var EURlabel = UILabel()
    var CNYlabel = UILabel()
    var dateLabel = UILabel()
    var currencies: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetch.fetchFromNBU(currency: "USD")
        view.backgroundColor = UIColor.CurrencyColor.backgroundDarkColor
        currencies = [firstCurrency, secondCurrency, thirdCurrency]
        
        dateLabel = UILabel.init(frame: CGRect(x: self.view.center.x - 100, y: 50, width: 200, height: 50))
        USDlabel = UILabel.init(frame: CGRect(x: self.view.center.x - 100, y: 100, width: 200, height: 100))
        EURlabel = UILabel.init(frame: CGRect(x: self.view.center.x - 100, y: 200, width: 200, height: 100))
        CNYlabel = UILabel.init(frame: CGRect(x: self.view.center.x - 100, y: 300, width: 200, height: 100))
        
        self.remakeLabels()
        self.view.addSubview(USDlabel)
        self.view.addSubview(EURlabel)
        self.view.addSubview(CNYlabel)
        self.view.addSubview(dateLabel)
        
        let bankButton = UIButton(frame: CGRect(x: self.view.center.x - 25, y: self.view.frame.height - 100, width: 50, height: 50))
        bankButton.setImage(UIImage.init(named: "bank"), for: UIControlState.normal)
        bankButton.addTarget(self, action: #selector(bankButtonPressed), for: UIControlEvents.touchUpInside)
        self.view.addSubview(bankButton)
        
        let gearButton = UIButton(frame: CGRect(x: self.view.center.x - 100, y: self.view.frame.height - 100, width: 50, height: 50))
        gearButton.setImage(UIImage.init(named: "gear"), for: UIControlState.normal)
        gearButton.addTarget(self, action: #selector(gearButtonPressed), for: UIControlEvents.touchUpInside)
        self.view.addSubview(gearButton)
        
        let chatButton = UIButton(frame: CGRect(x: self.view.center.x + 50, y: self.view.frame.height - 100, width: 50, height: 50))
        chatButton.setImage(UIImage.init(named: "chat"), for: UIControlState.normal)
        chatButton.addTarget(self, action: #selector(chatButtonPressed), for: UIControlEvents.touchUpInside)
        self.view.addSubview(chatButton)
        
    }
    
    func bankButtonPressed() {
        let alertController: CFAlertViewController = .alertController(title: "Source", message: "Choose source", textAlignment: .center, preferredStyle: .actionSheet, didDismissAlertHandler: nil)
        
        let privatBankAction: CFAlertAction = .action(title:"PrivatBank", style: .Default, alignment: .justified, backgroundColor: UIColor.CurrencyColor.backgroundDarkColor, textColor: UIColor.white) { (action) in
            self.fetch.source = FetchModel.FetchSource.bank
            self.currencies[2] = "RUR"
            self.remakeLabels()
            self.view.reloadInputViews() }
        alertController.addAction(privatBankAction)
        
        let nationalBankAction: CFAlertAction = .action(title: "National Bank of Ukraine", style: .Default, alignment: .justified, backgroundColor: UIColor.CurrencyColor.backgroundDarkColor, textColor: UIColor.white, handler: {
            (action) -> Void in
            self.fetch.source = FetchModel.FetchSource.nationalBank
            self.currencies[2] = "CNY"
            self.remakeLabels()
            self.view.setNeedsLayout()
        })
        alertController.addAction(nationalBankAction)
        
        let interBankAction: CFAlertAction = .action(title: "Interbank", style: .Default, alignment: .justified, backgroundColor: UIColor.CurrencyColor.backgroundDarkColor, textColor: UIColor.white, handler: {
            (action) -> Void in
            self.fetch.source = FetchModel.FetchSource.interBank
            self.currencies[2] = "RUB"
            self.remakeLabels()

        })
        alertController.addAction(interBankAction)
        
        let blackMarketAction: CFAlertAction = .action(title: "Black Market", style: .Default, alignment: .justified, backgroundColor: UIColor.CurrencyColor.backgroundDarkColor, textColor: UIColor.white, handler: {
            (action) -> Void in
            self.fetch.source = FetchModel.FetchSource.blackMarket
            self.fetch.fetchData(currency: self.currencies)
        })
        alertController.addAction(blackMarketAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func gearButtonPressed() {
    }
    
    func chatButtonPressed() {
    }
    
    func remakeLabels() {
        self.fetch.fetchData(currency: self.currencies)
        let labels: [UILabel] = [USDlabel, EURlabel, CNYlabel]
        var i = 0
        while i != (labels.count) {
            let name: String = currencies[i]
            let rates: [Double] = self.fetch.rates[name]!
            let buy: String = String(rates[0])
            let sale: String = String(rates[1])
            if buy == sale {
                labels[i].text = String(format: "%@\n%@", name, buy)
            } else {
                labels[i].text = String(format: "%@\n%@\t\t%@", name, buy, sale)
            }
            labels[i].numberOfLines = 2
            labels[i].textAlignment = .center
            labels[i].textColor = UIColor.CurrencyColor.fontBlueColor
            labels[i].font = UILabel().font.withSize(30.0)
            i += 1
        }
        dateLabel.text = fetch.dateOfFetch
        dateLabel.textColor = UIColor.CurrencyColor.currencySign
        dateLabel.font.withSize(17)
        dateLabel.textAlignment = .center
    }
}
