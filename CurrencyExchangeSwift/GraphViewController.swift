import UIKit
import Charts

class GraphViewController: UIViewController {
    @IBOutlet weak var usdGraph: LineChartView!
    @IBOutlet weak var eurGraph: LineChartView!
    @IBOutlet weak var cnyGraph: LineChartView!

    var fetch: FetchModel  = FetchModel.shared()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.CurrencyColor.backgroundDarkColor
        
        usdGraph.frame = CGRect(x: 0, y: 30, width: self.view.frame.width, height: self.view.frame.height/3.5 - 8)
        self.makeGraph(currency: "USD", graph: usdGraph)
        
        eurGraph.frame = CGRect(x: 0, y: 190, width: self.view.frame.width, height: self.view.frame.height/3.5 - 8)
        self.makeGraph(currency: "EUR", graph: eurGraph)
        
        cnyGraph.frame = CGRect(x: 0, y: 350, width: self.view.frame.width, height: self.view.frame.height/3.5 - 8)
        self.makeGraph(currency: "CNY", graph: cnyGraph)
    }
    override open func viewWillAppear(_ animated: Bool) {
        usdGraph.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        eurGraph.animate(xAxisDuration: 1.1, yAxisDuration: 1.1)
        cnyGraph.animate(xAxisDuration: 1.2, yAxisDuration: 1.2)
    }
    
    func makeGraph(currency: String, graph: LineChartView) {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM.dd"
        let date = formatter.string(from: Date())
        var dates: Array<String> = []
        for i in 0...6 {
            let cache = Double(date)!
            var numberToAppend = cache.roundTo(places: 2) - ((Double(i)/100).roundTo(places: 2))
            numberToAppend = numberToAppend.roundTo(places: 2)
            dates.append(String(numberToAppend))
        }
        var currencyRate: [Double] = []
        for i in FetchModel.shared().fetchGraphData(currency: currency) {
            currencyRate.append(i.roundTo(places: 2))
        }
        var ySeries: Array<ChartDataEntry> = []
        for i in 0...6 {
            let object = ChartDataEntry(x: Double(dates[i])!, y: currencyRate[i])
            ySeries.append(object)
        }
        let dataset = LineChartDataSet(values: ySeries, label: String(format: "%@/UAH", currency))
        let data = LineChartData(dataSets: [dataset])
        graph.data = data
        
        graph.gridBackgroundColor = NSUIColor.CurrencyColor.backgroundDarkColor
        graph.backgroundColor = UIColor.CurrencyColor.backgroundDarkColor
        graph.chartDescription?.text = ""
        graph.pinchZoomEnabled = false
        graph.doubleTapToZoomEnabled = false
        graph.dragEnabled = false
        graph.dragDecelerationEnabled = false
        graph.xAxis.drawGridLinesEnabled = false
        graph.xAxis.labelTextColor = UIColor.CurrencyColor.fontBlueColor
        graph.leftAxis.labelTextColor = UIColor.CurrencyColor.fontBlueColor
        graph.rightAxis.labelTextColor = UIColor.init(white: 0, alpha: 0)
        graph.xAxis.resetCustomAxisMax()
    }
}
