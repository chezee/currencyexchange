import UIKit

extension Double {
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension UIColor {
    struct CurrencyColor {
        static let backgroundDarkColor = UIColor(ciColor: CIColor(red: 0.070588, green: 0.070588, blue: 0.1098))
        static let fontBlueColor = UIColor(ciColor: CIColor(red: 0.0117, green: 0.9804, blue: 0.9453))
        static let currencySign = UIColor(ciColor: CIColor(red: 0.2549, green: 0.2549, blue: 0.2549))
    }
}
